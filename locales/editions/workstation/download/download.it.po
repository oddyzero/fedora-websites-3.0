#. extracted from content/editions/workstation/download.yml
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-26 13:33-0700\n"
"PO-Revision-Date: 2022-11-09 21:36+0000\n"
"Last-Translator: Nathan <nathan95@live.it>\n"
"Language-Team: Italian <https://translate.fedoraproject.org/projects/"
"fedora-websites-3-0/editionsworkstationdownload/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Translate Toolkit 3.7.3\n"

#: path
msgid "download"
msgstr "Scarica"

#: title
msgid "Download Fedora Workstation 37"
msgstr ""

#: description
msgid ""
"We're so glad you've decided to give Fedora Workstation a try. We know "
"you'll love it."
msgstr ""
"Siamo lieti che abbiate deciso di provare Fedora Workstation. Sappiamo che "
"vi piacerà."

#: header_images-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_background.jpg"
msgstr ""

#: sections-%3E[0]-%3EsectionTitle
msgid "release"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Etitle
msgid "RELEASE DATE"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Edescription
msgid "April 19, 2022"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Etitle
msgid "SUPPORTED THROUGH"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Edescription
msgid "May 17, 2023"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[2]-%3Etitle
msgid "Release Notes"
msgstr "Note di rilascio"

#: sections-%3E[0]-%3Econtent-%3E[2]-%3Elink-%3Eurl
#, read-only
msgid "https://fedorapeople.org/groups/schedule/f-37/f-37-all-tasks.html"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[3]-%3Etitle
msgid "Installation Guide"
msgstr "Guida all'installazione"

#: sections-%3E[0]-%3Econtent-%3E[3]-%3Elink-%3Eurl
#, read-only
msgid "https://docs.fedoraproject.org/en-US/fedora/latest/install-guide/"
msgstr ""

#: sections-%3E[0]-%3Econtent-%3E[4]-%3Etitle
msgid "Community Support"
msgstr "Supporto della comunità"

#: sections-%3E[0]-%3Econtent-%3E[4]-%3Elink-%3Eurl
#, read-only
msgctxt "sections->[0]->content->[4]->link->url"
msgid "https://ask.fedoraproject.org/"
msgstr ""

#: sections-%3E[1]-%3EsectionTitle
msgid "On Windows or macOS?"
msgstr "Su Windows o macOS?"

#: sections-%3E[1]-%3EsectionDescription
msgid ""
"Get started by using Fedora Media Writer, which makes it super easy to give "
"Fedora a try."
msgstr ""
"Per iniziare, utilizza Fedora Media Writer, che ti permette di provare "
"Fedora con estrema facilità."

#: sections-%3E[1]-%3Eimages
msgid "public/assets/images/media-writer-icon.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Etitle
msgid "Windows"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Elink-%3Etext
msgid "fa-windows"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Elink-%3Eurl
#, read-only
msgid "https://getfedora.org/fmw/FedoraMediaWriter-win32-latest.exe"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Etitle
msgid "Mac"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Elink-%3Etext
msgid "fa-apple"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Elink-%3Eurl
#, read-only
msgid "https://getfedora.org/fmw/FedoraMediaWriter-osx-latest.dmg"
msgstr ""

#: sections-%3E[2]-%3EsectionTitle
msgid "On Linux? Or just want an ISO file?"
msgstr "Su Linux? Volete solamente un file ISO?"

#: sections-%3E[2]-%3EsectionDescription
msgid ""
"Not sure how to use these files? [Learn here](https://docs.fedoraproject.org/"
"en-US/quick-docs/creating-and-using-a-live-installation-image/)"
msgstr ""

#: sections-%3E[3]-%3EsectionTitle
msgid "Security, Installs"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Etitle
msgid "We take security seriously"
msgstr "Prendiamo sul serio la sicurezza"

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Once you have downloaded an image, be sure to verify it for both security "
"and integrity."
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Elink-%3Eurl
#, read-only
msgid "https://getfedora.org/security/"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Elink-%3Etext
msgid "Verify Your Download"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Etitle
msgid "But wait! There's more."
msgstr "Ma aspettate! C'è di più."

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Need something a bit different? [Check out our other Fedora Workstation "
"downloads here](https://alt.fedoraproject.org/), featuring secondary "
"architectures and network-based installation images."
msgstr ""
"Avete bisogno di qualcosa di diverso? [Date un'occhiata agli altri download "
"di Fedora Workstation qui](https://alt.fedoraproject.org/), con architetture "
"secondarie e immagini di installazione basate sulla rete."

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Elink-%3Eurl
#, read-only
msgid "https://download.fedoraproject.org"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Elink-%3Etext
msgid "Fedora 37 64-bit network install"
msgstr ""

#: sections-%3E[4]-%3EsectionTitle
msgid "Laptops pre-loaded with Fedora"
msgstr "Laptop con Fedora preinstallato"

#: sections-%3E[4]-%3EsectionDescription
msgid ""
"We've partnered with companies like Lenovo to bring you laptops pre-loaded "
"with Fedora that include fully-supported hardware components. "
msgstr ""
"Abbiamo collaborato con aziende come Lenovo per offrivi laptop con Fedora "
"preinstallato che includono componenti hardware completamente supportati. "

#: sections-%3E[4]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/lenovo-laptop.png"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[0]-%3Elink-%3Eurl
#, read-only
msgid "https://fedoramagazine.org/lenovo-fedora-now-available/"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[0]-%3Elink-%3Etext
msgid "Explore Fedora Laptops"
msgstr ""

#: sections-%3E[5]-%3EsectionTitle
msgid "Learn more about Fedora Media Writer"
msgstr "Leggi di più a proposito di Fedora Media Writer"

#: sections-%3E[5]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Getting going with Fedora is easier than ever. All you need is a 2GB USB "
"flash drive, and Fedora Media Writer.\n"
"Once Fedora Media Writer is installed, it will set up your flash drive to "
"run a \"Live\" version of Fedora Workstation, meaning that you can boot it "
"from your flash drive and try it out right away without making any permanent "
"changes to your computer. Once you are hooked, installing it to your hard "
"drive is a matter of clicking a few buttons*."
msgstr ""

#: sections-%3E[5]-%3Econtent-%3E[1]-%3Edescription
msgid ""
" * Fedora requires a minimum of 20GB disk, 2GB RAM, to install and run "
"successfully. Double those amounts is recommended. "
msgstr ""
" * Fedora richiede un minimo di 20 GB di disco e 2 GB di RAM per essere "
"installato ed eseguito con successo. Si consiglia di raddoppiare queste "
"quantità. "

#: sections-%3E[5]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/monitor-2.png"
msgstr "public/assets/images/monitor-2.png"

#: sections-%3E[6]-%3EsectionTitle
msgid "Become a Fedora contributor"
msgstr ""

#: sections-%3E[6]-%3EsectionDescription
msgid ""
"Once you've got Fedora installed and running, why not join and contribute to "
"one of our online communities?"
msgstr ""

#: sections-%3E[7]-%3EsectionTitle
msgid "Officially Supported Fedora Community Spaces"
msgstr ""

#: sections-%3E[7]-%3Econtent-%3E[0]-%3Etitle
msgid "Fedora Discussion"
msgstr ""

#: sections-%3E[7]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/fedora-discussion-plus-icon.png"
msgstr ""

#: sections-%3E[7]-%3Econtent-%3E[0]-%3Elink-%3Eurl
#, read-only
msgid "https://discussion.fedoraproject.org/"
msgstr ""

#: sections-%3E[7]-%3Econtent-%3E[1]-%3Etitle
msgid "Fedora Chat"
msgstr "Chat di Fedora"

#: sections-%3E[7]-%3Econtent-%3E[1]-%3Elink-%3Eurl
#, read-only
msgid "https://matrix.to/#/#matrix-meta-chat:fedoraproject.org"
msgstr ""

#: sections-%3E[7]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/fedora-chat-plus-element.png"
msgstr ""

#: sections-%3E[7]-%3Econtent-%3E[2]-%3Etitle
msgid "Ask Fedora"
msgstr "Ak Fedora"

#: sections-%3E[7]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/ask-fedora.png"
msgstr ""

#: sections-%3E[7]-%3Econtent-%3E[2]-%3Elink-%3Eurl
#, read-only
msgctxt "sections->[7]->content->[2]->link->url"
msgid "https://ask.fedoraproject.org/"
msgstr ""

#: sections-%3E[8]-%3EsectionTitle
msgid "Community-Maintained Spaces"
msgstr "Spazi gestiti dalla comunità"

#: sections-%3E[8]-%3Econtent-%3E[0]-%3Etitle
msgid "Reddit"
msgstr "Reddit"

#: sections-%3E[8]-%3Econtent-%3E[0]-%3Elink-%3Eurl
#, read-only
msgid "https://www.reddit.com/r/Fedora/"
msgstr ""

#: sections-%3E[8]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/reddit-brands-3.png"
msgstr ""

#: sections-%3E[8]-%3Econtent-%3E[1]-%3Etitle
msgid "Discord"
msgstr "Discord"

#: sections-%3E[8]-%3Econtent-%3E[1]-%3Elink-%3Eurl
#, read-only
msgid "https://discord.com/invite/fedora"
msgstr ""

#: sections-%3E[8]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/discord-brands-2.png"
msgstr ""

#: sections-%3E[8]-%3Econtent-%3E[2]-%3Etitle
msgid "Telegram"
msgstr ""

#: sections-%3E[8]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/telegram-brands.png"
msgstr ""

#: sections-%3E[8]-%3Econtent-%3E[2]-%3Elink-%3Eurl
#, read-only
msgid "https://fedoraproject.org/wiki/Telegram"
msgstr ""

#: sections-%3E[9]-%3EsectionTitle
msgid ""
"By clicking on and downloading Fedora, you agree to comply with the "
"following terms and conditions."
msgstr ""
"Facendo clic e scaricando Fedora, si accetta di rispettare i seguenti "
"termini e condizioni."

#: sections-%3E[9]-%3EsectionDescription
msgid ""
"By downloading Fedora software, you acknowledge that you understand all of "
"the following: Fedora software and technical information may be subject to "
"the U.S. Export Administration Regulations (the “EAR”) and other U.S. and "
"foreign laws and may not be exported, re-exported or transferred (a) to any "
"country listed in Country Group E:1 in Supplement No. 1 to part 740 of the "
"EAR (currently, Cuba, Iran, North Korea, Sudan & Syria); (b) to any "
"prohibited destination or to any end user who has been prohibited from "
"participating in U.S. export transactions by any federal agency of the U.S. "
"government; or (c) for use in connection with the design, development or "
"production of nuclear, chemical or biological weapons, or rocket systems, "
"space launch vehicles, or sounding rockets, or unmanned air vehicle systems. "
"You may not download Fedora software or technical information if you are "
"located in one of these countries or otherwise subject to these "
"restrictions. You may not provide Fedora software or technical information "
"to individuals or entities located in one of these countries or otherwise "
"subject to these restrictions. You are also responsible for compliance with "
"foreign law requirements applicable to the import, export and use of Fedora "
"software and technical information."
msgstr ""
"Scaricando il software Fedora, l'utente dichiara di aver compreso quanto "
"segue: Il software Fedora e le informazioni tecniche possono essere soggetti "
"alle normative statunitensi sull'amministrazione delle esportazioni (l'\"EAR"
"\") e ad altre leggi statunitensi e straniere e non possono essere "
"esportati, riesportati o trasferiti (a) in nessuno dei Paesi elencati nel "
"Gruppo di Paesi E:1 nel Supplemento n. 1 alla parte 740 dell'EAR ("
"attualmente, Cuba, Iran, Corea del Nord, Sudan e Siria); (b) a qualsiasi "
"destinazione proibita o a qualsiasi utente finale a cui sia stato proibito "
"di partecipare a transazioni di esportazione negli Stati Uniti da qualsiasi "
"agenzia federale del governo degli Stati Uniti; o (c) per l'uso in relazione "
"alla progettazione, allo sviluppo o alla produzione di materiali nucleari, "
"di sviluppo o di produzione. (c) per l'uso in relazione alla progettazione, "
"allo sviluppo o alla produzione di armi nucleari, chimiche o biologiche, o "
"di sistemi a razzo, veicoli di lancio spaziale, razzi sonda o sistemi di "
"veicoli aerei senza equipaggio. Non potete scaricare il software Fedora o le "
"informazioni tecniche se vi trovate in uno di questi Paesi o se siete "
"altrimenti soggetti a queste restrizioni. Non è possibile fornire il "
"software Fedora o le informazioni tecniche a persone o entità situate in uno "
"di questi Paesi o comunque soggette a queste restrizioni. Lei è inoltre "
"responsabile del rispetto dei requisiti di legge stranieri applicabili "
"all'importazione, all'esportazione e all'uso del software Fedora e delle "
"informazioni tecniche."

#~ msgid "Security, Installs, Laptops Preloaded"
#~ msgstr "Sicurezza, installazione, Laptop preinstallati"

#~ msgid "Officially-Supported Fedora Community Spaces"
#~ msgstr "Spazi della comunità Fedora supportati ufficialmente"

#~ msgid "Download Fedora Workstation 37."
#~ msgstr "Scarica Fedora Workstation 37."

#~ msgid "Download Options"
#~ msgstr "Opzioni di download"

#~ msgid ""
#~ "Not sure how to use these files? [Learn More](https://docs.fedoraproject.org/"
#~ "en-US/quick-docs/creating-and-using-a-live-installation-image/)"
#~ msgstr ""
#~ "Non sapete come utilizzare questi file? [Per saperne di più](https://docs."
#~ "fedoraproject.org/en-US/quick-docs/"
#~ "creating-and-using-a-live-installation-image/)"

#~ msgid ""
#~ "Once you have downloaded an image, be sure to verify it for both security "
#~ "and integrity. "
#~ msgstr ""
#~ "Una volta scaricata un'immagine, assicurarsi di verificarne la sicurezza e "
#~ "l'integrità. "

#~ msgid "Verify your download"
#~ msgstr "Verifica il tuo download"

#~ msgid "Explore Fedora laptops"
#~ msgstr "Esplora i Laptop Fedora"

#~ msgid "/r/fedora"
#~ msgstr "/r/fedora"

#~ msgid "Fedora Discord"
#~ msgstr "Fedora Discord"

#~ msgctxt "sections->[4]->content->[2]->title"
#~ msgid "Telegram"
#~ msgstr "Telegram"

#~ msgctxt "sections->[4]->content->[2]->link->text"
#~ msgid "Telegram"
#~ msgstr "Telegram"

#~ msgid ""
#~ "Getting going with Fedora is easier than ever. All you need is a 2GB USB "
#~ "flash drive, and Fedora Media Writer.\n"
#~ "\n"
#~ "Once Fedora Media Writer is installed, it will set up your flash drive to "
#~ "run a \"Live\" version of Fedora Workstation, meaning that you can boot it "
#~ "from your flash drive and try it out right away without making any permanent "
#~ "changes to your computer. Once you are hooked, installing it to your hard "
#~ "drive is a matter of clicking a few buttons*. "
#~ msgstr ""
#~ "Iniziare a usare Fedora è più facile che mai. Tutto ciò che serve è una "
#~ "chiavetta USB da 2 GB e Fedora Media Writer.\n"
#~ "\n"
#~ "Una volta installato Fedora Media Writer, la chiavetta verrà configurata per "
#~ "eseguire una versione \"Live\" di Fedora Workstation, il che significa che "
#~ "potrete avviarla dalla chiavetta e provarla subito senza apportare modifiche "
#~ "permanenti al vostro computer. Una volta che si è pronti, l'installazione "
#~ "sul disco rigido è una questione di pochi clic*. "

#~ msgid "Download Fedora Workstation 35."
#~ msgstr "Scarica Fedora 35 workstation."
