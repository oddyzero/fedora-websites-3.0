#. extracted from content/editions/workstation/home.yml
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-26 13:33-0700\n"
"PO-Revision-Date: 2022-12-10 08:20+0000\n"
"Last-Translator: Mi Lachew <milachew@mail.lv>\n"
"Language-Team: Russian <https://translate.fedoraproject.org/projects/"
"fedora-websites-3-0/editionsworkstationhome/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Translate Toolkit 3.7.3\n"

#: path
msgid "index"
msgstr "индекс"

#: title
msgid "The leading Linux desktop"
msgstr "Ведущая настольная система Linux"

#: description
msgid ""
"A beautiful, high-quality desktop, built on the latest open source "
"technology. Trusted, powerful and easy.\n"
msgstr ""
"Красивая, высококачественная настольная система, созданная на основе "
"новейших технологий с открытым исходным кодом. Проверенная, мощная, удобная."
"\n"

#: header_images-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_framework.png"
msgstr ""

#: header_images-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_background.jpg"
msgstr ""

#: links-%3E[0]-%3Etext
msgid "Download now"
msgstr "Скачать сейчас"

#: links-%3E[0]-%3Eurl
#, read-only
msgid "/workstation/download"
msgstr ""

#: links-%3E[1]-%3Etext
msgid "Reviewed By TechHut"
msgstr "Обзор от TechHut"

#: links-%3E[1]-%3Eurl
#, read-only
msgid "https://www.youtube.com/watch?v=54aoIGKEMnk"
msgstr ""

#: sections-%3E[0]-%3EsectionTitle
msgid "Why Fedora Workstation?"
msgstr "Почему именно Fedora Workstation?"

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Etitle
msgid "Reliable"
msgstr "Надёжная"

#: sections-%3E[0]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Each version is updated for 18 months, and upgrades between versions are "
"quick and easy."
msgstr ""
"Каждая версия обновляется в течение 18 месяцев, а обновления между версиями "
"выполняются быстро и легко."

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Etitle
msgid "Free & private"
msgstr "Свободная и приватная"

#: sections-%3E[0]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"With Fedora, your desktop is your own. It's free, there are no ads, and your "
"data belongs to you."
msgstr ""
"С Fedora ваш компьютер будет именно вашим. Это бесплатно, без рекламы, и "
"ваши данные принадлежат вам."

#: sections-%3E[0]-%3Econtent-%3E[2]-%3Etitle
msgid "Beautiful"
msgstr "Красивая"

#: sections-%3E[0]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Workstation is carefully curated to deliver a high-quality experience. The "
"desktop is clean and uncluttered."
msgstr ""
"Fedora Workstation тщательно продумана, чтобы обеспечить высокое качество "
"работы. Рабочий стол чист и лаконичен."

#: sections-%3E[0]-%3Econtent-%3E[3]-%3Etitle
msgid "Trusted"
msgstr "Проверенная"

#: sections-%3E[0]-%3Econtent-%3E[3]-%3Edescription
msgid ""
"Developed in partnership with upstream projects. Rigorously tested. Backed "
"by Red Hat."
msgstr ""
"Разработана в партнерстве с ведущими проектами. Тщательно протестировано. "
"При поддержке Red Hat."

#: sections-%3E[0]-%3Econtent-%3E[4]-%3Etitle
msgid "Leading technology"
msgstr "Передовые технологии"

#: sections-%3E[0]-%3Econtent-%3E[4]-%3Edescription
msgid ""
"Built on the latest technologies and enhancements that open source has to "
"offer."
msgstr ""
"Создана на основе новейших технологий и улучшений, которые может предложить "
"открытый исходный код."

#: sections-%3E[0]-%3Econtent-%3E[5]-%3Etitle
msgid "Makes the most of your device"
msgstr "По максимуму использует возможности вашего устройства"

#: sections-%3E[0]-%3Econtent-%3E[5]-%3Edescription
msgid ""
"Fedora works with hardware vendors to make excellent hardware support across "
"a range of devices."
msgstr ""
"Fedora сотрудничает с поставщиками оборудования, чтобы обеспечить "
"превосходную поддержку оборудования различных устройств."

#: sections-%3E[1]-%3EsectionTitle
msgid "Features for everyone"
msgstr "Возможности для каждого"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Etitle
msgid "Fantastic app"
msgstr "Фантастические приложения"

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"Browse a great collection of apps, covering everything you might need. "
"Includes exciting new open source apps as well as familiar apps from other "
"platforms."
msgstr ""
"Большая коллекция приложений, охватывающая всё, что вам может понадобиться. "
"Включает захватывающие новые приложения с открытым исходным кодом, а также "
"знакомые приложения с других платформ."

#: sections-%3E[1]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_software_center.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Etitle
msgid "Workstation speaks your language"
msgstr "Workstation говорит на вашем языке"

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Thanks to a global community of translators, Workstation is available in "
"many languages."
msgstr ""
"Благодаря глобальному сообществу переводчиков Workstation доступна на многих "
"языках."

#: sections-%3E[1]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_languages.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Etitle
msgid "Turn the lights down low"
msgstr "Уменьшите яркость"

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Flip a switch to turn on dark mode and give your eyes a break from the day. "
"Use night light to reduce screen glare and help with sleep."
msgstr ""
"Используйте переключатель, чтобы включить тёмный режим и дать глазам "
"отдохнуть от дневного света. Используйте ночной свет, чтобы уменьшить блики "
"на экране и облегчить отход ко сну."

#: sections-%3E[1]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_nightlight.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[3]-%3Etitle
msgid "Calendar integration"
msgstr "Интеграция с календарём"

#: sections-%3E[1]-%3Econtent-%3E[3]-%3Edescription
msgid "Bring your online calendar to the desktop with online accounts"
msgstr ""
"Перенесите свой онлайн-календарь на рабочий стол из сетевых учётных записей"

#: sections-%3E[1]-%3Econtent-%3E[3]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_calendar.png"
msgstr ""

#: sections-%3E[1]-%3Econtent-%3E[4]-%3Etitle
msgid "Helpful out of the box"
msgstr "Весь функционал из коробки"

#: sections-%3E[1]-%3Econtent-%3E[4]-%3Edescription
msgid ""
"Fedora Workstation includes a great set of utilities, like Clocks, Weather "
"and Maps."
msgstr ""
"Fedora Workstation включает в себя отличный набор утилит, таких как Часы, "
"Погода и Карты."

#: sections-%3E[1]-%3Econtent-%3E[4]-%3Eimage
#, read-only
msgid "public/assets/images/workstation_outofthebox.png"
msgstr ""

#: sections-%3E[2]-%3EsectionTitle
msgid "Great for developers"
msgstr "Отлично подходит для разработчиков"

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Etitle
msgid "Virtualization made easy"
msgstr "Виртуализация стала проще"

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Edescription
msgid "Boxes takes the guesswork out of using virtual machines."
msgstr ""
"Boxes избавляет вас от построения догадок при использовании виртуальных "
"машин."

#: sections-%3E[2]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/dev1.png"
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[1]-%3Etitle
msgid "Productivity-boosting desktop features"
msgstr "Функции рабочего стола, повышающие продуктивность"

#: sections-%3E[2]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Use performance mode to boost hardware speed when you need it. Turn off "
"notifications with Do Not Disturb. Press the Super key and just type to "
"search for what you need."
msgstr ""
"Используйте режим производительности, чтобы ускорить оборудование, когда вам "
"это нужно. Отключите уведомления с помощью «Не беспокоить». Нажмите клавишу "
"Super и просто введите то, что хотите найти."

#: sections-%3E[2]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/dev2.png"
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[2]-%3Etitle
msgid "Containers ready to go"
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Use the latest container tools from the Red Hat ecosystem. Not setup "
"required. Access the Red Hat container registry."
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/dev3.png"
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[3]-%3Etitle
msgid "All the tools"
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[3]-%3Edescription
msgid ""
"All the tools you might need, easy to install with a single command. Tools "
"supplied with high quality, by the people who make them."
msgstr ""

#: sections-%3E[2]-%3Econtent-%3E[3]-%3Eimage
#, read-only
msgid "public/assets/images/dev4.png"
msgstr ""

#: sections-%3E[3]-%3EsectionTitle
msgid "Get started developing with Fedora Workstation"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"The Fedora Developer portal has guides on developing and deploying "
"applications using Fedora workstation."
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Eurl
#, read-only
msgctxt "sections->[3]->content->[0]->url"
"sections->[3]->content->[0]->url"
msgid "https://fedoradeveloper.com"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[0]-%3Eimage
#, read-only
msgid "public/assets/images/fedora-devs.png"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"DNF is the Fedora package manager that enables you to install, update and "
"manage OS components."
msgstr ""
"DNF — это менеджер пакетов Fedora, который позволяет вам устанавливать, "
"обновлять и управлять компонентами ОС."

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Eurl
#, read-only
msgctxt "sections->[3]->content->[1]->url"
msgid "https://fedoradeveloper.com"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[1]-%3Eimage
#, read-only
msgid "public/assets/images/logo-dnf.png"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Podman is a daemonless container engine for creating and managing containers "
"and container images."
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[2]-%3Eurl
#, read-only
msgctxt "sections->[3]->content->[2]->url"
msgid "https://fedoradeveloper.com"
msgstr ""

#: sections-%3E[3]-%3Econtent-%3E[2]-%3Eimage
#, read-only
msgid "public/assets/images/podman.svg"
msgstr ""

#: sections-%3E[4]-%3EsectionTitle
msgid "Built by you"
msgstr ""

#: sections-%3E[4]-%3Eimages
msgid "public/assets/images/iot_flock_background.jpg"
msgstr ""

#: sections-%3E[4]-%3EsectionDescription
msgid ""
"The Fedora Project envisions a world where everyone can benefit from free "
"and open source software built by inclusive, welcoming, and open-minded "
"communities.\n"
"Fedora Workstation is created by a team in the Fedora community called the **"
"Workstation Working Group**. It is comprised of official members who have "
"decision making powers, as well as other contributors. [Learn more onthe "
"Workstation Working Group Website](https://docs.fedoraproject.org/en-US/"
"workstation-working-group/)"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[0]-%3Etitle
msgid "Report & discuss issues"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[0]-%3Edescription
msgid ""
"You can view, file, and discuss Fedora Workstation issues on [the Fedora "
"Workstation issue tracker]()."
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[1]-%3Etitle
msgid "Chat with the team"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[1]-%3Edescription
msgid ""
"Visit **#fedora-workstation** on [irc.libera.chat]() and on **#workstation** "
"[on the Fedora Matrix Chat server]()."
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[2]-%3Etitle
msgid "Join the mailing list"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[2]-%3Edescription
msgid ""
"Sign up at [desktop@lists.fedoraproject.org]() to receive meeting agendas "
"and minute ([view archives]().)"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[3]-%3Etitle
msgid "Attend a meeting"
msgstr ""

#: sections-%3E[4]-%3Econtent-%3E[3]-%3Edescription
msgid ""
"Open to all current and potential contributors on **Tuesdays, 10:00 AM US "
"Eastern** on [Bluejeans]()."
msgstr ""

#~ msgid "Fantastic apps"
#~ msgstr "Фантастические приложения"

#~ msgid "Fedora Workstation"
#~ msgstr "Fedora Workstation"

#~ msgid "The flagship desktop distribution of the Fedora Project"
#~ msgstr "Флагманский настольный дистрибутив проекта Fedora"

#~ msgid "Latest Release"
#~ msgstr "Последний выпуск"

#~ msgid "36"
#~ msgstr "36"

#~ msgid "Release Notes<https://getfedora.org>"
#~ msgstr "Примечания к выпуску <https://getfedora.org>"

#~ msgid "fa-youtube"
#~ msgstr "fa-youtube"

#, read-only
#~ msgid "https://www.youtube.com/results?search_query=fedora+workstation+review"
#~ msgstr "https://www.youtube.com/results?search_query=fedora+workstation+review"

#~ msgid ""
#~ "A beautiful, high-quality desktop, built on the latest open source "
#~ "technology. Trusted, powerful and easy."
#~ msgstr ""
#~ "Красивая, высококачественная настольная система, созданная на основе "
#~ "новейших технологий с открытым исходным кодом. Проверенная, мощная, удобная."

#~ msgid "Benefits"
#~ msgstr "Преимущества"
